import unittest
import sys
sys.path.append("../../leg-endary/")

import datahandling.grf as grf

class TestGRF(unittest.TestCase):

    # Positive Case
    def test_positive(self):
        self.assertEqual(grf.get_GRF("Anterior/Posterior", 0.1, "XS"), -0.0913496762514114)
        self.assertEqual(grf.get_GRF("Anterior/Posterior", 0.81, "XS"), 0.0)
        self.assertEqual(grf.get_GRF("Vertical", 0.51, "L"), 0.0247313939034939)
        self.assertEqual(grf.get_GRF("Vertical", 0.81, "XS"), 0.0)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.12, "M"), 1.13318634033203)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.57, "S"), 0.566854655742645)    
    
    # Negative Case
    def test_negative(self):
        self.assertEqual(grf.get_GRF("ERROR", 0.81, "XS"), None)
        self.assertEqual(grf.get_GRF("ERROR", 0.32, "L"), None)
        self.assertEqual(grf.get_GRF("Vertical", "ERROR", "XS"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", "ERROR", "L"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.4, "ERROR"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.23, "ERROR"), None)

    # Null Case
    def test_null(self):
        self.assertEqual(grf.get_GRF("ERROR", 0.81, "XS"), None)
        self.assertEqual(grf.get_GRF("ERROR", 0.32, "L"), None)
        self.assertEqual(grf.get_GRF("Vertical", "ERROR", "XS"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", "ERROR", "L"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.4, "ERROR"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.23, "ERROR"), None)

if __name__ == '__main__':
    unittest.main()