import unittest
import sys
sys.path.append("../../leg-endary/")

import sensors.sensor_calibration as sc

class TestSensorCalibration(unittest.TestCase):

    # Positive Case
    def test_positive(self):
        self.assertEqual(sc.calibrate([[4, 2, 1], [1, 2, 5], [1, 2, 3]], 3), (2.0, 2.0, 3.0))
        self.assertEqual(sc.calibrate([[1, 65, 1], [1, 2, 5], [1,15, 32]], 3), (1.0, 27.333333333333332, 12.666666666666666))
        self.assertEqual(sc.calibrate([[7, 3, 1], [1, 23, 55], [1, 2, 33]], 3)1§, (3.0, 9.333333333333334, 29.666666666666668))
  
    '''
    # Negative Case
    def test_negative(self):
        self.assertEqual(grf.get_GRF("ERROR", 0.81, "XS"), None)
        self.assertEqual(grf.get_GRF("ERROR", 0.32, "L"), None)
        self.assertEqual(grf.get_GRF("Vertical", "ERROR", "XS"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", "ERROR", "L"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.4, "ERROR"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.23, "ERROR"), None)

    # Null Case
    def test_null(self):
        self.assertEqual(grf.get_GRF("ERROR", 0.81, "XS"), None)
        self.assertEqual(grf.get_GRF("ERROR", 0.32, "L"), None)
        self.assertEqual(grf.get_GRF("Vertical", "ERROR", "XS"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", "ERROR", "L"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.4, "ERROR"), None)
        self.assertEqual(grf.get_GRF("Medio/Lateral", 0.23, "ERROR"), None)
    '''


if __name__ == '__main__':
    unittest.main()
