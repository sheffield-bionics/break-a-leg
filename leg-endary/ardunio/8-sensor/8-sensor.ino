#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ICM20X.h>
#include <Adafruit_ICM20948.h>

#define TCAADDR 0x70

/* Assign a unique ID to this sensor at the same time */
Adafruit_ICM20948 s [8];

void tcaselect(uint8_t i) {
  if (i > 7) return;
 
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();  
}


void setup(void) 
{
  Serial.begin(115200);

  for (int i = 0; i < 8; i++) 
  {
    tcaselect(i);
    if (!s[i].begin_I2C()) {
      Serial.print("sensor not detected");
    }
    s[i].setAccelRange(ICM20948_ACCEL_RANGE_16_G);
    s[i].setGyroRange(ICM20948_GYRO_RANGE_2000_DPS);
    s[i].setGyroRateDivisor(255);
  }
}

void loop(void) 
{
  for (int i = 0; i < 8; i++)
  {
     tcaselect(i);
     // s[i].getEvent(&event);
     
     sensors_event_t accel;
     sensors_event_t gyro;
     sensors_event_t mag;
     sensors_event_t temp;
     s[i].getEvent(&accel, &gyro, &temp, &mag);
      
     Serial.print("s" + i);
     Serial.print(",");
    
     Serial.print(accel.acceleration.x);
     Serial.print(","); Serial.print(accel.acceleration.y);
     Serial.print(","); Serial.print(accel.acceleration.z);
   
     Serial.print(",");
     Serial.print(gyro.gyro.x);
     Serial.print(","); Serial.print(gyro.gyro.y);
     Serial.print(","); Serial.print(gyro.gyro.z);
      
     Serial.print(",");
     Serial.print(mag.magnetic.x);
     Serial.print(","); Serial.print(mag.magnetic.y);
     Serial.print(","); Serial.print(mag.magnetic.z);
    
     Serial.println();
      
     delay(50);
  }
  
}
