import serial
from helpers import realtime as t
from sensors import Sensor as s
from datahandling import io
import numpy as np
from GUI import Graph as g 

# Config
CALIBRATION_DEPTH = 15
PORT = "/dev/cu.usbserial-10"
SENSORS = 8

sensors = { f"s{i}": s.Sensor(f"s{i}", CALIBRATION_DEPTH) for i in range(0, SENSORS)}
print(sensors)

def __read(arduino_data):
	# Reads ardunino data from serial port
	data = arduino_data.readline()
	data = str(data, 'utf-8').split("\r")[0].split(",")
	return data
	

def __initialize():
	# Sets up serial transmission from arduino
	print(f"{t.t()} Sheffield Bionics Leg Firmware v1.0")
	print(f"{t.t()} Input Port: {PORT}")
	print(f"{t.t()} Sensors: {SENSORS}")
	print(f"{t.t()} Calibration Depth: {CALIBRATION_DEPTH}")
	return serial.Serial(PORT, 115200)


def __calibrate():
	print(f"{t.t()} Starting sensor calibration")

	calibration_data = []
	while (len(calibration_data) < CALIBRATION_DEPTH):
		calibration_data.append(__read(arduino_data))

	cX, cY, cZ = sc.calibrate(calibration_data, CALIBRATION_DEPTH)
	print(cX, cY, cZ)
	sensor.update_calibration(cX, cY, cZ)

	print(f"{t.t()} Finished sensor calibration")


def main():
	arduino_data = __initialize()

	__calibrate()

	print(f"{t.t()} Generating training instance")

	path = io.new_single_data_instance("X", [cX, cY, cZ])

	# Sensors data in the format
	# aX,aY,aZ
	while True:
		while (arduino_data.inWaiting() == 0):
			pass
		aX, aY, aZ = __read(arduino_data)

		print(f"{aX} {aY} {aZ}")
		io.append_sensor_data(path, [aX, aY, aZ])
		#sensor.update_data(float(aX), float(aY), float(aZ))


if __name__ == "__main__":
	main()