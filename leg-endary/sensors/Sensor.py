from threading import Thread

class Sensor():
	def __init__(self, identifer, calibration_depth):
		super()

		# Sensor Identification
		self.identifer = identifer

		# IMU Data
		self.acce = Vector3(0, 0, 0)
		self.gyro = Vector3(0, 0, 0)
		self.magn = Vector3(0, 0, 0)

		# Calibration Offset Data
		self.acce_o = Vector3(0, 0, 0)
		self.gyro_o = Vector3(0, 0, 0)
		self.magn_o = Vector3(0, 0, 0)

		# Calibration Settings
		self.calibration_instance = 0
		self.calibration_depth = calibration_depth


	def calibrate(acce, gyro, magn):
		if (is_calibrated()):
			return
			
		self.acce_o += acce
		self.gyro_o += gyro
		self.magn_o += magn

		self.calibration_instance += 1

		if (is_calibrated()):
			self.acce_o /= self.calibration_instance
			self.gyro_o /= self.calibration_instance
			self.magn_o /= self.calibration_instance


	def is_calibrated():
		return (self.calibration_instance >= self.calibration_depth)


	def update_data(self, acce, gyro, magn):
		if (is_calibrated() == False):
			return None
		self.acce = acce
		self.gyro = gyro
		self.magn = magn
		print(f"{str(self.aX - self.aX_offset)[:6]} {str(self.aY - self.aY_offset)[:6]} {str(self.aZ - self.aZ_offset)[:6]}")