# leg-firmware/docs/GUI/Graph.md

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

class Graph:

	def __init__(self, data_x, data_y, interval=None, xlim=None, ylim=None, xlabel=None, ylabel=None, label=None):
		self.data_x = data_x
		self.data_y = data_y
		self.interval = interval
		self.xlim = xlim
		self.ylim = ylim
		self.xlabel = xlabel
		self.ylabel = ylabel
		self.label = label
	
	def animate(i):
		# Update xlim and ylim
		if (self.xlim != None):
			plt.xlim(self.xlim[0], self.xlim[1])

		if (self.ylim != None):
			plt.ylim(self.ylim[0], self.ylim[1])

		# Clear graph and plot new data
		plt.cla()
		if (self.label != None):
			plt.plot(self.data_x, self.data_y, label=self.label)
		else:
			plt.plot(self.data_x, self.data_y)
		# Updates key
		plt.legend(loc='upper left')
		

	def setup(self):
		plt.style.use('fivethirtyeight')

		# Set xlabel and ylabel
		if (self.xlabel != None):
			plt.xlabel(self.xlabel)

		if (self.ylabel != None):
			plt.ylabel(self.ylabel)
 
		FuncAnimation(plt.gcf(), self.animate, interval=1000)


	

	def update_data(self, data_x, data_y):
		self.data_x = data_x
		self.data_y = data_y
		plt.show()

